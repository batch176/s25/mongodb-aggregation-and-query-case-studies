
//#1

db.Fruits.aggregate([

    {$match:{supplier:"Red Farms Inc."}},
    {$count: "itemSupplied"}
])


//#2
db.Fruits.aggregate([

    {$match:{price:{$gt:50}}},
    {$count: "itemPriceGreater50"}
])


//#3
db.Fruits.aggregate([

    {$match:{onSale:true}},
    {$group: {_id:"$supplier", avgPrice:{$avg: "$price"}}}
])

//#4
db.Fruits.aggregate([

    {$match:{onSale:true}},
    {$group: {_id:"$supplier", maxPricePerSupplier:{$max: "$price"}}}
])

//#5
db.Fruits.aggregate([

    {$match:{onSale:true}},
    {$group: {_id:"$supplier", lowestPricePerSupplier:{$min: "$price"}}}
])